import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:json_app/data/constants.dart';

class Exchanges extends StatefulWidget {
  Exchanges({Key? key}) : super(key: key);

  @override
  State<Exchanges> createState() => _ExchangesState();
}

class _ExchangesState extends State<Exchanges> {
  final String cryptoExchanges = CRYPTO_EXCHANGES;
  final String cryptoExchanges2 = CRYPTO_EXCHANGES;
  var textController = TextEditingController();
  List<dynamic> _exchanges = [];
  List<dynamic> _searchResult = [];
  @override
  void initState() {
    _exchanges = jsonDecode(cryptoExchanges);
    _searchResult = jsonDecode(cryptoExchanges);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: textController,
            decoration: InputDecoration(
              label: Text("Ingrese su busqueda"),
              suffixIcon: Icon(Icons.send),
              prefixIcon: Icon(Icons.search_sharp),
              border: OutlineInputBorder(),
            ),
            onChanged: onSearchTextChanged,
          ),
        ),
        Expanded(
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
            itemCount: _searchResult.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: ListTile(
                    leading: Image.network("${_searchResult[index]["image"]}"),
                    title: Text("${_searchResult[index]["name"]}"),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${_searchResult[index]["country"]}"),
                        Text("${_searchResult[index]["year_established"]}"),
                      ],
                    ),
                    trailing: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          color: Colors.blueGrey, shape: BoxShape.circle),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "${_searchResult[index]["trust_score"]}",
                          style: TextStyle(color: Colors.white, fontSize: 15),
                        ),
                      ),
                    )),
              );
            },
          ),
        )
      ],
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      _searchResult = _exchanges;
      _exchanges = jsonDecode(cryptoExchanges);
    } else {
      _exchanges.forEach((exch) {
        if (exch["name"].toLowerCase().contains(text)) _searchResult.add(exch);
      });
    }

    setState(() {});
  }
}
