import 'package:flutter/material.dart';
import 'package:json_app/exchanges.dart';
import 'package:json_app/home_page.dart';

class Trendings extends StatefulWidget {
  Trendings({Key? key}) : super(key: key);

  @override
  State<Trendings> createState() => _TrendingsState();
}

class _TrendingsState extends State<Trendings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Crypto APP'),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                  child: Container(
                color: Colors.blueGrey,
              )),
              ListTile(
                title: Text("Trending"),
                onTap: () {},
              ),
              ListTile(
                title: Text("Exchanges"),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => HomePage()));
                },
              ),
            ],
          ),
        ));
  }
}
