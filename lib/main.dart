import 'package:flutter/material.dart';
import 'package:json_app/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        title: 'Material App',
        home: HomePage());
  }
}
