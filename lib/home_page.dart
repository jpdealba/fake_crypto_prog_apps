import 'package:flutter/material.dart';
import 'package:json_app/exchanges.dart';
import 'package:json_app/trendings.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Crypto APP'),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                  child: Container(
                color: Colors.blueGrey,
              )),
              ListTile(
                title: Text("Trending"),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => Trendings()));
                },
              ),
              ListTile(
                title: Text("Exchanges"),
                onTap: () {},
              ),
            ],
          ),
        ),
        body: Exchanges());
  }
}
